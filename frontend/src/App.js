import React from 'react';
import HomePage from "./containers/HomePage/HomePage";
import Register from "./containers/Register/Register";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import InviteForm from "./containers/InviteForm/InviteForm";
import NewEvent from "./containers/NewEvent/NewEvent";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <ProtectedRoute
                    exact
                    path="/"
                    component={HomePage}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/invite"
                    component={InviteForm}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/newEvent"
                    component={NewEvent}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;