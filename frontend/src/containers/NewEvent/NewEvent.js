import React, {useState} from 'react';
import {Button, Grid, TextField} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {addEvent} from "../../store/actions/eventsActions";

const NewEvent = () => {
    const dispatch = useDispatch();
    const [state, setState] = useState({
        eventDate: "",
        eventDuration: "",
        eventText: ""
    });
    const error = useSelector(state => state.events.eventError);

    const onChangeHandler = (e) => {
        const {name, value} = e.target;
        setState(state => ({
            ...state,
            [name]: value
        }));
    }

    const addButton = () => {
        dispatch(addEvent({...state}));
    }

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid container style={{paddingTop: "20px"}} spacing={2} direction="column">
            <Grid item>
                <TextField
                    id="date"
                    label="Date"
                    type="date"
                    name="eventDate"
                    onChange={onChangeHandler}
                    value={state.eventDate}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    error={getFieldError('eventDate')}
                />
            </Grid>
            <Grid item>
                <FormElement
                    label="Text"
                    onChange={onChangeHandler}
                    name="eventText"
                    value={state.eventText}
                    error={getFieldError('eventText')}
                />
            </Grid>
            <Grid item>
                <FormElement
                    label="Duration"
                    onChange={onChangeHandler}
                    name="eventDuration"
                    value={state.eventDuration}
                    error={getFieldError('eventDuration')}
                />
            </Grid>
            <Grid item>
                <Button
                    type="button"
                    onClick={() => addButton()}
                    variant="contained"
                    color="primary"
                >
                    Add
                </Button>
            </Grid>
        </Grid>
    );
};

export default NewEvent;