import React from 'react';
import {Alert, AlertTitle} from "@material-ui/lab";
import {Button, Card, CardHeader, Grid} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {removeFriend} from "../../../store/actions/usersActions";

const FriendsList = ({friends}) => {
    const dispatch = useDispatch();

    return (
        <>
            <Alert severity="info">
                <AlertTitle>Friends</AlertTitle>
                <Grid container spacing={1}>
                    {friends.length !== 0 ? friends.map(i => (
                        <Grid item style={{width: "100%"}} key={i._id}>
                            <Card style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                                width: "500px",
                                alignItems: "center",
                                padding: "0 10px 0 10px"
                            }}>
                                <CardHeader title={i.displayName}/>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    onClick={() => dispatch(removeFriend(i._id))}
                                >
                                    remove
                                </Button>
                            </Card>
                        </Grid>
                    )) : <Grid item><h2>No friends here</h2></Grid> }
                </Grid>
            </Alert>
        </>
    );
};

export default FriendsList;