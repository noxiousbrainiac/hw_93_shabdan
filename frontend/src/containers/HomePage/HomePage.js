import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {clearError, fetchFriends} from "../../store/actions/usersActions";
import {Backdrop, Button, Grid} from "@material-ui/core";
import {historyPush} from "../../store/actions/historyActions";
import {fetchEvents} from "../../store/actions/eventsActions";
import EventList from "./EventList/EventList";
import FriendsList from "./FriendsList/FriendsList";

const HomePage = () => {
    const dispatch = useDispatch();
    const friends = useSelector(state => state.users.friends);
    const events = useSelector(state => state.events.events);
    const [open, setOpen] = useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen(!open);
    };

    useEffect(() => {
        dispatch(fetchFriends());
        dispatch(fetchEvents());

        return () => {
            dispatch(clearError());
        }
    }, [dispatch]);

    return (
        <>
            <Grid container style={{paddingTop: "20px"}}>
                <Grid item>
                    <h2>Event List</h2>
                    <Button
                        style={{marginRight: "20px"}}
                        onClick={() => dispatch(historyPush('/invite'))}
                        variant="contained"
                        color="primary"
                    >
                        Invite
                    </Button>

                    <Button
                        style={{marginRight: "20px"}}
                        onClick={() => dispatch(historyPush('/newEvent'))}
                        variant="contained"
                        color="primary"
                    >
                        Add
                    </Button>

                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleToggle}
                    >
                        Friends
                    </Button>
                </Grid>
            </Grid>
            <Grid container>
                <EventList events={events}/>
            </Grid>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
                onClick={handleClose}
            >
                <FriendsList friends={friends}/>
            </Backdrop>
        </>
    );
};

export default HomePage;