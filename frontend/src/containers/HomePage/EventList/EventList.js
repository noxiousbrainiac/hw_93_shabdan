import React from 'react';
import EventItem from "./EventItem/EventItem";
import {Grid} from "@material-ui/core";

const EventList = ({events}) => {
    return (
        <Grid container spacing={2} style={{paddingTop: "20px"}} direction="column">
            {events.length !== 0 ? events.map(i => (
                    <EventItem
                        key={i._id}
                        text={i.eventText}
                        duration={i.eventDuration}
                        date={i.eventDate}
                        id={i._id}
                    />
                ))
                : <h2>No events here</h2>
            }
        </Grid>
    );
};

export default EventList;