import React from 'react';
import {Button, Card, CardHeader, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteEvent} from "../../../../store/actions/eventsActions";

const EventItem = ({date, text, duration, id}) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();

    const remove = () => {
        dispatch(deleteEvent(id));
    }

    return (
        <Grid item>
            <Card style={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-evenly"}}>
                <CardHeader title={date}/>
                <p>Note: {text}</p>
                <p>Duration: {duration}</p>
                <span>Posted by: {user.displayName}</span>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={remove}
                >
                    remove
                </Button>
            </Card>
        </Grid>
    );
};

export default EventItem;