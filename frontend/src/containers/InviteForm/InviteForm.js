import React, {useState} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Grid} from "@material-ui/core";
import {share} from "../../store/actions/usersActions";
import {useDispatch} from "react-redux";

const InviteForm = () => {
    const [state, setState] = useState({email: ""});
    const dispatch = useDispatch();

    const onChangeHandler = (e) => {
        const {name, value} = e.target;
        setState(state => ({
            ...state,
            [name]: value
        }));
    };

    const onAddButton = () => {
        dispatch(share({...state}));
    }

    return (
        <Grid container style={{paddingTop: "20px"}} direction="column" spacing={2}>
            <Grid item>
                <FormElement
                    label="Add friend to share (email)"
                    onChange={(e) => onChangeHandler(e)}
                    name="email"
                    value={state.email}
                />
            </Grid>
            <Grid item>
                <Button
                    onClick={onAddButton}
                    type="button"
                    variant="contained"
                    color="primary"
                >
                    Add
                </Button>
            </Grid>
        </Grid>
    );
};

export default InviteForm;