import usersSlice from "../slices/usersSlice";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";

export const {
    registerUser,
    registerUserSuccess,
    registerUserFailure,
    loginUser,
    loginUserSuccess,
    loginUserFailure,
    logout,
    clearError,
    fetchFriends,
    fetchFriendsSuccess,
    fetchFriendsFailure,
    share,
    shareFailure,
    shareSuccess,
    removeFriendSuccess,
    removeFriend,
    removeFriendFailure
} = usersSlice.actions;

export const facebookLogin = data => async (dispatch) => {
    try {
        const response = await axiosApi.post('/users/facebookLogin', data);
        dispatch(loginUserSuccess(response.data.user));
        dispatch(historyPush('/'));
        toast.success('Login successful');
    } catch (error) {
        toast.error(error.response.data.global);
        dispatch(loginUserFailure(error.response.data));
    }
};