import eventsSlice from "../slices/eventsSlice";

export const {
    addEvent,
    addEventSuccess,
    addEventFailure,
    fetchEventsSuccess,
    fetchEvents,
    fetchEventsFailure,
    deleteEvent,
    deleteEventFailure,
    deleteEventSuccess,
} = eventsSlice.actions;