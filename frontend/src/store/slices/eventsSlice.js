import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    events: [],
    eventLoading: false,
    eventError: null,
};

const name = "events";

const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        addEvent(state, action) {
            state.eventLoading = true;
        },
        addEventSuccess(state, action) {
            state.eventLoading = false;
        },
        addEventFailure(state, action) {
            state.eventLoading = false;
            state.eventError = action.payload;
        },
        fetchEvents(state, action) {
            state.eventLoading = true;
        },
        fetchEventsSuccess(state, action) {
            state.eventLoading = false;
            state.events = action.payload;
        },
        fetchEventsFailure(state, action) {
            state.eventLoading = false;
            state.eventError = action.payload;
        },
        deleteEvent(state, action) {
            state.eventLoading = true;
        },
        deleteEventSuccess(state, action) {
            state.eventLoading = false;
        },
        deleteEventFailure(state, action) {
            state.eventLoading = false;
            state.eventError = action.payload;
        },
    }
});

export default eventsSlice;