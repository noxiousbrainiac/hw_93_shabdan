import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    user: null,
    registerLoading: false,
    registerError: null,
    loginError: null,
    loginLoading: false,
    friends: [],
    error: null,
    loading: false,
    shareError: null,
    shareLoading: false,
    friend: {}
};

const name = 'users';

const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        registerUser(state, action) {
            state.registerLoading = true;
        },
        registerUserSuccess(state, {payload: userData}) {
            state.user = userData;
            state.registerLoading = false;
            state.registerError = null;
        },
        registerUserFailure(state, action) {
            state.registerLoading = false;
            state.registerError = action.payload;
        },
        loginUser(state, action) {
            state.loginLoading = true;
        },
        loginUserSuccess(state, action) {
            state.loginLoading = false;
            state.user = action.payload;
            state.loginError = null;
        },
        loginUserFailure(state, action) {
            state.loginError = action.payload;
            state.loginLoading = false;
        },
        clearError(state, action) {
            state.loginError = null;
            state.registerError = null;
            state.error = null;
        },
        logout(state, action) {
            state.user = null;
        },
        fetchFriends(state, action) {
            state.loading = true;
        },
        fetchFriendsSuccess(state, action) {
            state.loading = false;
            state.friends = action.payload;
        },
        fetchFriendsFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        share(state, action) {
            state.shareLoading = true;
        },
        shareSuccess(state, action) {
            state.loading = false;
            state.friend = action.payload;
        },
        shareFailure(state, action) {
            state.shareLoading = false;
            state.shareError = action.payload;
        },
        removeFriend(state, action) {
            state.loading = true;
        },
        removeFriendSuccess(state, action) {
            state.loading = false;
        },
        removeFriendFailure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export default usersSlice;