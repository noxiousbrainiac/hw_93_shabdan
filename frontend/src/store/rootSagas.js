import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import eventsSagas from "./sagas/eventsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...eventsSagas
    ])
}