import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {
    addEvent,
    addEventFailure,
    addEventSuccess, deleteEvent, deleteEventFailure, deleteEventSuccess, fetchEvents,
    fetchEventsFailure,
    fetchEventsSuccess
} from "../actions/eventsActions";

export function* addEventSaga({payload: data}) {
    try {
        const response = yield axiosApi.post('/events', data);
        yield put(addEventSuccess(response.data));
        yield put(historyPush('/'));
    } catch (e) {
        toast.error(e.response.data.global);
        if (e.response && e.response.data) {
            yield put(addEventFailure(e.response.data));
        } else {
            yield put(addEventFailure({message: "No internet connexion"}));
        }
    }
}

export function* fetchEventsSaga() {
    try {
        const response = yield axiosApi.get('/events');
        yield put(fetchEventsSuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.global);
        if (e.response && e.response.data) {
            yield put(fetchEventsFailure(e.response.data));
        } else {
            yield put(fetchEventsFailure({message: "No internet connexion"}));
        }
    }
}

export function* deleteEventSaga({payload: id}) {
    try {
        const response = yield axiosApi.delete(`/events/${id}`);
        yield put(deleteEventSuccess());
        console.log(response.data);
        toast.success('Event deleted!');
    } catch (e) {
        toast.error("Cannot delete!");
        if (e.response && e.response.data) {
            yield put(deleteEventFailure(e.response.data));
        } else {
            yield put(deleteEventFailure({message: "No internet connexion"}));
        }
    }
}

const eventsSagas = [
    takeEvery(addEvent, addEventSaga),
    takeEvery(fetchEvents, fetchEventsSaga),
    takeEvery(deleteEvent, deleteEventSaga),
];

export default eventsSagas;