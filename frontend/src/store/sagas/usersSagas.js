import {put, takeEvery} from "redux-saga/effects";
import {
    fetchFriends,
    fetchFriendsFailure,
    fetchFriendsSuccess,
    loginUser,
    loginUserFailure,
    loginUserSuccess, logout,
    registerUser,
    registerUserFailure,
    registerUserSuccess, removeFriend, removeFriendFailure, removeFriendSuccess, share, shareFailure, shareSuccess
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Registered successful!');
    } catch (e) {
        toast.error(e.response.data.global);
        if (e.response && e.response.data) {
            yield put(registerUserFailure(e.response.data));
        } else {
            yield put(registerUserFailure({message: "No internet connexion"}));
        }
    }
}

export function* loginUserSaga({payload: user}) {
    try {
        const {data} = yield axiosApi.post('/users/sessions', user);
        yield put(loginUserSuccess(data));
        yield put(historyPush('/'));
        toast.success('Login successful!');
    } catch (e) {
        toast.error(e.response.data.global);
        if (e.response && e.response.data) {
            yield put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({message: "No internet connexion"}));
        }
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({message: "No internet connexion"}));
        }
    }
}

export function* fetchFriendsSaga() {
    try {
        const {data} = yield axiosApi.get('/users/friends');
        yield put(fetchFriendsSuccess(data));
    } catch (e) {
        toast.error(e.response.data.global);
        if (e.response && e.response.data) {
            yield  put(fetchFriendsFailure(e.response.data));
        } else {
            yield put(fetchFriendsFailure({message: "No internet connexion"}));
        }
    }
}

export function* shareSaga({payload: email}) {
    try {
        const {data} = yield axiosApi.post('/users/share', email);
        yield put(shareSuccess(data));
        yield put(historyPush('/'));
        toast.success("Shared !");
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(shareFailure(e.response.data));
        } else {
            yield put(shareFailure({message: "No internet connexion"}));
        }
    }
}

export function* removeFriendSaga({payload: id}) {
    try {
        const {data} = yield axiosApi.delete(`/users/${id}/share`);
        yield put(removeFriendSuccess());
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(removeFriendFailure(e.response.data));
        } else {
            yield put(removeFriendFailure({message: "No internet connexion"}));
        }
    }
}

const usersSaga = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(loginUser, loginUserSaga),
    takeEvery(logout, logoutUserSaga),
    takeEvery(fetchFriends, fetchFriendsSaga),
    takeEvery(share, shareSaga),
    takeEvery(removeFriend, removeFriendSaga)
];

export default usersSaga;