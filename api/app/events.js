const express = require('express');
const Event = require('../models/Event');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth ,async (req, res) => {
   try {
       const eventData = {
           eventDate: req.body.eventDate,
           eventText: req.body.eventText,
           eventDuration: req.body.eventDuration,
           user: req.user._id
       };

       const event = new Event(eventData);
       await event.save();
       res.send(event);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post('/share', auth, async (req, res) => {
    try {
        const events = await Event.find({user: req.user._id});

    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', auth ,async (req, res) => {
    try {
        if (req.user.friends.length > 0) {
            const events = [];

            const userEvents = await Event.find({user: req.user._id}).sort({eventDate: -1})
            events.push(...userEvents);

            req.user.friends.map(async (friend) => {
                const friendEvents = await Event.find({user: friend._id}).sort({eventDate: -1});
                return events.push(...friendEvents);
            });

            return res.send(events);
        }

        const events = await Event.find({user: req.user._id}).sort({eventDate: -1});
        res.send(events);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
   try {
       const event = await Event.findById(req.params.id);

       if (req.user.id.toString() !== event.user.toString()) {
           return res.status(403).send({error: "You can't delete this product"});
       }

       if (event) {
           await Event.findByIdAndDelete(req.params.id);
           res.send('Event deleted!');
       } else {
           res.status(404).send({message: "Event not found!"});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;