const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    eventDate: {
        type: String,
        required: true,
    },
    eventText: {
        type: String,
        required: true,
    },
    eventDuration: {
        type: String,
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;